# Project Templates

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)

<!-- ABOUT THE PROJECT -->
## About The Project

This is a template for all Code4Venezuela projects.

Your project should have all the following files:

- README such as this one with clear descriptions.
- LICENSE file. We use MIT license.
- CODEOWNERS file. Read more about them [here](https://docs.gitlab.com/ee/user/project/code_owners.html).
- gitlab-ci.yaml file for CI/CD. Read more [here](https://docs.gitlab.com/ee/ci/).


### Built With

- List your project's main frameworks and tools.

<!-- GETTING STARTED -->
## Getting Started

Briefly describe how a new developer can run the project and get started.


### Prerequisites

List the prerequisites and how to install them.


### Installation

More in depth description of how to install the package (if any) and
all the required set up for the project.

<!-- USAGE EXAMPLES -->
## Usage

Describe how to use the project.

<!-- ROADMAP -->
## Roadmap

Describe or link to any planned work for this project.:w


<!-- CONTRIBUTING -->
## Contributing

Contributions are welcomed and **appreciated**:

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

<!-- LICENSE -->
## License

All Code4Venezuela projects are distributed under the MIT License. See
`LICENSE` for more information.
